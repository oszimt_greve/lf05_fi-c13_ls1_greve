package AB_Auswahlstrukturen;
import java.util.Scanner;

public class Aufgabe_5
{
    private static final Scanner _SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        // AB Auswahlstrukturen – Aufgabe 5: BMI
        System.out.println("BMI Calculator\n");
        double height = getInput("Enter your height (in cm): ") / 100;
        double weight = getInput("Enter your weight (in kg): "); 
        char gender = getGender();
        double result = calculateBMI(weight, height);
        System.out.printf("%nBMI = %.2f", result);
        classification(result, gender);
        _SCANNER.close();  
    }

    public static double getInput(String question) {
        System.out.print(question);
        return _SCANNER.nextDouble();
    }

    public static char getGender() {
        System.out.print("Enter your gender (M/F): ");
        return _SCANNER.next().charAt(0);
    }

    public static double calculateBMI(double weight, double height) {
        double bmi = ((weight) / (Math.pow(height, 2)));
        return bmi;  
    }

    public static void classification(double bmi, char gender) {
        String result = ""; 

        if((bmi < 20 && gender == 'M') || (bmi < 19 && gender == 'F')) {
            result = "Classification: Underweight"; 
        } else if((bmi > 20 && bmi <= 25 && gender == 'M') || (bmi > 19 && bmi <= 24 && gender == 'F')) {
            result = "Classification: Normal"; 
        } else if((bmi > 25 && gender == 'M') || (bmi > 24 && gender == 'F')) {
            result = "Classification: Overweight"; 
        } else {
            result = "Request has missing or invalid parameters and cannot be parsed.";
        }

        System.out.printf("%n%n%s", result);
    }
}
