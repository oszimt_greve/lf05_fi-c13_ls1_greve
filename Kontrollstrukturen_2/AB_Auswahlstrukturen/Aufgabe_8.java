package AB_Auswahlstrukturen;
import java.util.Scanner;

public class Aufgabe_8
{
    public static void main(String[] args) {
        // AB Auswahlstrukturen – Aufgabe 8: Schaltjahr
        System.out.print("Enter the year (YYYY): "); 
		Scanner scanner = new Scanner(System.in);
		int year = scanner.nextInt();

        if(year < 1582) {
            if(year % 4 == 0) {
				System.out.println("\n" + year + " is a leap year.");
			}else {
				System.out.print("\n" + year + " is not a leap year."); 
			}
        }else if(year >= 1582) {
            if((year % 4 == 0) && (year % 100 != 0)) {
                System.out.println("\n" + year + " is a leap year.");
            }else if (year % 400 == 0) {
                System.out.println("\n" + year + " is a leap year.");
            }else if(year % 100 == 0) {
				System.out.println("\n" + year + " is not a leap year."); 
			}else {
                System.out.println("\n" + year + " is not a leap year.");
            }
        }

        scanner.close(); 
    }
}
