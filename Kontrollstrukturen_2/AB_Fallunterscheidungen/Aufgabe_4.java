package AB_Fallunterscheidungen;
import java.util.Scanner;

public class Aufgabe_4
{
	private static final Scanner _SCANNER = new Scanner(System.in);
	
	public static void main(String[] args) {
		// AB Fallunterscheidungen – Aufgabe 4: Taschenrechner
		System.out.println("Calculator\n");
		System.out.print("Enter the first number: ");
		double firstNumber = _SCANNER.nextDouble();
		System.out.print("Enter the second number: ");
		double secondNumber = _SCANNER.nextDouble();
		System.out.print("Choose an operator: "); 
		char operator = _SCANNER.next().charAt(0);
		double result; 
		
		switch(operator) { 
			case '+':
				result = firstNumber + secondNumber;
				System.out.println("\nResult: " + result); 
				break;
			case '-':
				result = firstNumber - secondNumber;
				System.out.println("\nResult: " + result); 
				break;
			case '*':
				result = firstNumber * secondNumber;
				System.out.println("\nResult: " + result); 
				break;
			case '/':
				result = firstNumber / secondNumber;
				System.out.println("\nResult: " + result); 
				break;
			default:
				System.out.println("\nRequest has missing or invalid parameters and cannot be parsed.");
				break; 
		}
		
		_SCANNER.close(); 
	}
}
