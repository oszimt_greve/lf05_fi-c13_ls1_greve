package AB_Fallunterscheidungen;

import java.util.Scanner;

public class Aufgabe_5
{
	private static final Scanner _SCANNER = new Scanner(System.in);
	
	public static void main(String[] args) {
		// AB Fallunterscheidungen – Aufgabe 5: Ohmsches Gesetz
		System.out.println("Ohm's Law\n");
		System.out.println("Which value would you like to calculate? (U/I/R): "); 
		char option = _SCANNER.next().charAt(0);
		double U, I, R; 
		
		switch(option) {
			case 'U':
				R = getInput("Enter a value for R: ");
				I = getInput("Enter a value for I: ");
				U = R * I;
				System.out.println("\nU = " + U);
				break;
			case 'R':
				U = getInput("Enter a value for U: ");
				I = getInput("Enter a value for I: ");
				R = U / I;
				System.out.println("\nR = " + R);
				break;
			case 'I':
				U = getInput("Enter a value for U: ");
				R = getInput("Enter a value for R: ");
				I = U / R;
				System.out.println("\nI = " + I);
				break;
			default:
				System.out.println("\nRequest has missing or invalid parameters and cannot be parsed.");
				break; 
		}
	}
	
	public static double getInput(String question) {
        System.out.print(question);
        return _SCANNER.nextDouble();
    }
}
