package AB_Schleifen_1;

public class Aufgabe_5
{
	public static void main(String[] args) {
		// AB Schleifen 1 – Aufgabe 5: Einmaleins
		for(int i = 1; i <= 10; i++) {
			for(int c = 1; c <= 10; c++) {
				System.out.print(i * c);
				System.out.print(" ");
			}
			System.out.println(); 
		}
	}
}
