package AB_Schleifen_1;

import java.util.Scanner;

public class Aufgabe_8
{
	public static void main(String[] args) {
		// AB Schleifen 1 – Aufgabe 8: Quadrat
		System.out.println("Square Generator\n");
		System.out.print("Enter the square size: ");
		Scanner scanner = new Scanner(System.in);
		int squareSize = scanner.nextInt();
		System.out.println();
		
		for(int i = 0; i <= squareSize; i++) {
			for(int c = 0; c <= squareSize; c++) {
				if(i == 0 || c == 0 || i == squareSize || c == squareSize) {
					System.out.print("* "); 
				}else {
					System.out.print("  "); 
				}
			}
			System.out.println(); 
		}
		
		scanner.close(); 
	}
}
