﻿// 3. Vergleichen Sie die neue Implementierung mit der alten 
// und erläutern Sie die Vor- und Nachteile der jeweiligen Implementierung

/*
 * Der Vorteil besteht darin, dass nun viel leichter Optionen zur Auswahl hinzugefügt werden können, 
 * weil durch das Array alle Einträge dynamisch geworden sind; die Fehlermeldungen müssen also nicht 
 * mehr angepasst werden, weil die Länge direkt vom Array kommt und außerdem lassen sich so allgemein 
 * schneller Einträge hinzufügen. 
 * 
 * Der Nachteil in dieser Implementierung besteht darin, dass alle Referenzen des Arrays im Quellcode 
 * geändert werden müssen, sofern Änderungsbedarf im bereits bestehendem Array besteht - das gilt 
 * natürlich nicht für neue Einträge, die in das Array hinzugefügt werden. 
 */

import java.util.Scanner;

public class Fahrkartenautomat
{
	public static void main(String[] args) {
		double zuZahlen = fahrkartenbestellungErfassen();
	    double bezahlen = fahrkartenBezahlen(zuZahlen);
	    fahrkartenAusgeben(250);
	    rueckgeldAusgeben(zuZahlen, bezahlen);
	}
	
	static final Scanner _SCANNER = new Scanner(System.in);
	
	private static double fahrkartenbestellungErfassen() {
		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("=========================\n");
		
		double ticketpreis = 0;
		int ticketanzahl;
		
		String[] fahrkartenoptionen = {
		    "1. Einzelfahrschein Berlin AB", "2. Einzelfahrschein Berlin BC" , "3. Einzelfahrschein Berlin ABC", 
		    "4. Kurzstrecke", "5. Tageskarte Berlin AB", "6. Tageskarte Berlin BC", "7. Tageskarte Berlin ABC", 
		    "8. Kleingruppen-Tageskarte Berlin AB", "9. Kleingruppen-Tageskarte Berlin BC", 
		    "10. Kleingruppen-Tageskarte Berlin ABC"
		};
		
    	double[] fahrkartenpreise = {2.90 , 3.30 , 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	
    	for(int i = 0; i < fahrkartenoptionen.length ; i++) {
    		System.out.printf("%s%s%.2f%s\n",  fahrkartenoptionen[i], " für ", fahrkartenpreise[i], " € ");
    	}
		
		System.out.print("\nIhre Wahl: ");
		
		int wahl = _SCANNER.nextInt();
		
		while(wahl < 1 || wahl > fahrkartenoptionen.length) {
    		System.out.println("  >> Wählen Sie bitte eine Zahl von 1 bis " + fahrkartenoptionen.length + " aus.");
    		System.out.print("\nIhre Wahl: ");
    		wahl = _SCANNER.nextInt();
    	}
		
		int ticketpreisberechnung = wahl - 1;
		ticketpreis = fahrkartenpreise[ticketpreisberechnung];
		
		System.out.print("Anzahl der Tickets: ");
    	ticketanzahl = _SCANNER.nextInt();
    	
        if(ticketanzahl < 1 || ticketanzahl > 10) {
        	System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis" + fahrkartenoptionen + " Tickets aus.");
        	System.out.print("\nIhre Wahl: ");
    		ticketanzahl = _SCANNER.nextInt();
        }
        
        return ticketanzahl * ticketpreis; 
	}
	
	private static double fahrkartenBezahlen(double zuZahlen) {
		double eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlen)
	    { 
		    System.out.printf("%s%.2f%s\n", "Noch zu zahlen: ", zuZahlen - eingezahlterGesamtbetrag, " €"); 
		    System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
		    double eingeworfeneMünze = _SCANNER.nextDouble();
		    eingezahlterGesamtbetrag += eingeworfeneMünze; 
	    }
		return eingezahlterGesamtbetrag;
	}
	
	private static void fahrkartenAusgeben(int zeit) {
		System.out.println("\nIhr Fahrschein wird ausgegeben (Bitte warten Sie einen Moment).\n");
		for(int i = 0; i < 3; i++) {
			System.out.print(". ");
			try {
	            Thread.sleep(zeit);
	        } catch (InterruptedException exception) {
	            exception.printStackTrace();
	        }
		}
		System.out.println("\n");
	}
	
	private static double rueckgeldAusgeben(double zuZahlen, double eingezahlterGesamtbetrag) {
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
		if(rückgabebetrag > 0.0)
	    {
			System.out.printf("%s%.2f%s", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, " € ");
		    System.out.println("wird in folgenden Münzen ausgezahlt: ");

		    while(rückgabebetrag >= 2.0) 
		    {
		    	System.out.println("2.00 €");
			    rückgabebetrag = rückgabebetrag - 2.0;
		    }
		    while(rückgabebetrag >= 1.0) 
		    {
		    	System.out.println("1.00 €");
			    rückgabebetrag = rückgabebetrag - 1.0;
		    }
		    while(rückgabebetrag >= 0.5) 
		    {
		    	System.out.println("0.50 €");
			    rückgabebetrag = rückgabebetrag - 0.5;
		    }
		    while(rückgabebetrag >= 0.2)
		    {
		    	System.out.println("0.20 €");
			    rückgabebetrag = rückgabebetrag - 0.2; 
		    }
		    while(rückgabebetrag >= 0.1) 
		    {
		    	System.out.println("0.10 €");
			    rückgabebetrag = rückgabebetrag - 0.1;
		    }
		    while(rückgabebetrag >= 0.05)
		    {
		    	System.out.println("0.05 Euro");
			    rückgabebetrag = rückgabebetrag - 0.05;
		    }
	    }
		return rückgabebetrag;
	}
}
