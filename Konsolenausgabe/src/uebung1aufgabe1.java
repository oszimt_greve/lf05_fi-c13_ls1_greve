public class uebung1aufgabe1
{
	public static void main(String[] args)
	{
		System.out.print("Asuka is best girl. ");
		System.out.print("Asuka is best girl.");
		
		System.out.print("\n\nAsuka is best \"girl\"." + "\nAsuka is best girl.");
		
		/* Die print Methode gibt Text auf der Konsole aus und f�gt keine neue Zeile hinzu. 
		 * Die println Methode f�gt eine neue Zeile hinzu, nachdem Text auf der Konsole ausgegeben wurde.
		 * Die print Methode funktioniert nur, wenn ein Eingabearameter �bergeben wird, ganz im Gegensatz
		 * zur println Methode, welche auch ohne Eingabe eines Parameters funktioniert. 
		 * */
	}

}
