public class Temperaturtabelle
{
	public static void main(String[] args)
		System.out.printf("%s  |  %s\n", "Fahrenheit", " Celsius");
		System.out.print("-".repeat(23) + "\n");
		System.out.printf("%d" + " ".repeat(9) + "|" + " ".repeat(4) + "%.2f", -20, -28.8889);
		System.out.printf("\n%d" + " ".repeat(9) + "|" + " ".repeat(4) + "%.2f", -10, -23.3333);
		System.out.printf("\n%+d" + " ".repeat(10) + "|" + " ".repeat(5) + "%.2f", 0, -6.6667);
		System.out.printf("\n%+d" + " ".repeat(9) + "|" + " ".repeat(4) + "%.2f", 20, -28.8889);
		System.out.printf("\n%+d" + " ".repeat(9) + "|" + " ".repeat(5) + "%.2f", 30, -1.1111);
	}
}
