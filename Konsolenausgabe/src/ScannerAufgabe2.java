import java.util.Scanner;

public class ScannerAufgabe2
{
	public static void main(String[] args)
	{
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Hallo. Gib deinen Vornamen ein: ");
		
		String vorname = myScanner.nextLine();
		
		System.out.println("Jetzt gib dein Alter ein: ");
		
		byte alter = myScanner.nextByte();
		
		System.out.println("Dein Vorname lautet " + vorname + " und du bist " + alter + " Jahre alt");
		
		myScanner.close();
	}	
}
