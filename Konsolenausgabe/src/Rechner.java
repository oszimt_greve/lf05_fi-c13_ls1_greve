import java.util.Scanner;

public class Rechner
{
	public static void main(String[] args)
	{
		// Neues Scanner-Objekt myScanner wird erstellt
		 Scanner myScanner = new Scanner(System.in);
		 
		 System.out.print("Bitte geben Sie eine ganze Zahl ein: "); 
		 int zahl1 = myScanner.nextInt(); 
		 
		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		 int zahl2 = myScanner.nextInt();

		 int ergebnisAddition = zahl1 + zahl2; 
		 int ergebnisMultiplikation = zahl1 * zahl2;
		 float ergebnisDivision = (float) zahl1 / zahl2;
		 int ergebnisSubtraktion = zahl1 - zahl2;
		 
		 System.out.print("\n\n\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisAddition); 
		 
		 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
		 System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisMultiplikation); 
		 
		 System.out.print("\n\n\nErgebnis der Division lautet: ");
		 System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisDivision); 
		 
		 System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
		 System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisSubtraktion); 

		 myScanner.close();
	}
}
